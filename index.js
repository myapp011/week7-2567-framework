const express = require("express");
const app = express();
const port = process.env.PORT || 3001

require("dotenv").config()

http: app.get("/", (req,res) => {
    return res.status(200).json({message:"GET request to the Home page"})
})

http: app.post("/", (req,res) => {
    return res.status(201).json({message:"POST request to the Home page"})
})

http: app.put("/", (req,res) => {
    return res.status(200).json({message:"PUT request to the Home page"})
})

app.listen(port, () => {

    console.log(`Server running at http://localhost:${port}`);

})